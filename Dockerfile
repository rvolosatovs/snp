FROM docker.io/rust:1.57 as sevctl
RUN cargo install --root /usr/local --bin sevctl -- sevctl

FROM registry.gitlab.com/enarx/lab:latest

# Install and configure the kernel
ADD linux.tar.bz2 /
RUN cd /boot; ln -s vmlinuz* wyrcan.kernel

# Install SEV firmware
COPY amd_sev_fam17h_model0xh.sbin /lib/firmware/amd/
COPY amd_sev_fam17h_model3xh.sbin /lib/firmware/amd/
COPY amd_sev_fam19h_model0xh.sbin /lib/firmware/amd/
RUN sed -i 's|main|main non-free|' /etc/apt/sources.list
RUN apt update \
 && apt install -y --no-install-recommends amd64-microcode \
 && rm -rf /var/lib/apt/lists/*

# Enable SEV and set SEV limits
COPY kvm-amd.conf /etc/modprobe.d/kvm-amd.conf
COPY sev.conf /etc/security/limits.d/sev.conf
COPY 99-sev.rules /etc/udev/rules.d/

# Compile sevctl and install service
COPY --from=sevctl /usr/local/bin/sevctl /usr/local/bin/
COPY sevctl.service /etc/systemd/system/sevctl.service
RUN mkdir -p /var/cache/amd-sev \
    && systemctl enable sevctl.service
